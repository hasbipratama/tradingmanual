<?php
// Menjalankan fungsi session
session_start();
// jika session masuk belum dibuat maka kembalikan kehalaman masuk
if (!isset($_SESSION["masuk"])) {
    header("location: masuk.php");
    exit;
}
// Menghubungkan dengan file function
require 'function/functions.php';
// Menampung nilai dari variabel $_GET
$id = $_GET["id"];

if (hapusm30($id)) {
    echo "
        <script>
        alert('Data berhasil dihapus');
        document.location.href = 'index.php';
        </script>";
} else {
    echo "
    <script>
    alert('Data gagal dihapus');
    document.location.href = 'index.php';
    </script>
    ";
    echo "<br>";
    echo "mysqli_error($id);";
}
