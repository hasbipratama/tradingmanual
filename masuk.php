<?php
// Menjalankan fungsi session start
session_start();
// Mengkoneksikan dengan file function
require 'function/functions.php';
// Jika cookie id dan value masih ada 
if (isset($_COOKIE['id']) && isset($_COOKIE['value'])) {
    // Menyimpan sementara nilai id dan valie pada variabel
    $id = $_COOKIE["id"];
    $key = $_COOKIE["value"];
    // Mengquery username dari database dimana id sama dengan id yang diinputkan pada login
    $result = mysqli_query($conn, "SELECT username FROM user WHERE id = $id");

    $row = mysqli_fetch_assoc($result);

    if ($key === hash("sha224", $row["username"])) {
        $_SESSION['masuk'] = true;
    }
}
// Mengembalikan ke index jika session login sudah dibuat
if (isset($_SESSION['masuk'])) {
    header("location: index.php");
    exit;
}
// Jika tombol masuk sudah ditekan
if (isset($_POST["masuk"])) {
    // Menyimpan variabel username dan password yang diinputkan user
    $username = $_POST["username"];
    $password = $_POST["password"];
    // query username kedatabase
    $result =  mysqli_query($conn, "SELECT * FROM user WHERE username = '$username'");
    // Jika username ditemukan akan mendapatkan nilai 1 row
    if (mysqli_num_rows($result) === 1) {
        // Mendapatkan password dari kolom username diawal
        $row = mysqli_fetch_assoc($result);
        // Mencocokkan password yang diinputkan dengan yang ada didatabase
        if (password_verify($password, $row['password'])) {
            // Set Session
            $_SESSION['masuk'] = true;
            // Mengambil nilai ingat
            if (isset($_POST['ingat'])) {
                // set cookie
                setcookie('id', $row['id'], time() + 60);
                setcookie('value', hash('sha224', $row['username']), time() + 60);
            }
            // Mengarahkan ke halaman index jika username dan password benar
            header('location: index.php');
            exit;
        }
    }
    $error = true;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/masuk1.css">
    <title>Masuk</title>
</head>

<body>
    <form action="" method="POST">
        <h1>Masuk</h1>
        <?php
        if (isset($error)) : ?>
            <p style="color: red; font-style:italic; text-align:center;">Username /Password salah!</p>
        <?php endif; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4  offset-md-4">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" name="username" id="username" required autofocus autocomplete="off">
                </div>
            </div>
            <div class=" row">
                <div class="col-sm-4 offset-sm-4">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" required name="password">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 offset-sm-4">
                    <input type="checkbox" class="form-check-input" name="ingat" id="checkbox">
                    <label class="form-check-label" for="checkbox">Ingat saya</label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 offset-4">
                    <button type="submit" class="btn btn-success" name="masuk">Masuk</button>
                    <hr>
                    <p>Belum memiliki akun?</p>
                    <a href="daftar.php"><button type="button" class="btn btn-danger">Buat akun</button></a>
                </div>
            </div>
        </div>
    </form>
</body>

</html>