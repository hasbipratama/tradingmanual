<?php
//Koneksi kedatabases
$conn = mysqli_connect("localhost", "root", "", "tradingmanual");
//Function query tabel H4
function queryh4($query)
{
    // Menngambil koneksi database dari variabel $conn diatas
    global $conn;
    //mengambil data dari tabel h4
    $result = mysqli_query($conn, $query);
    $h4s = [];
    while ($h4 = mysqli_fetch_assoc($result)) {
        $h4s[] = $h4;
    };
    return $h4s;
}
//Function query tabel M30
function querym30($query1)
{
    // Menngambil koneksi database dari variabel $conn diatas
    global $conn;
    //mengambil data dari tabel h4
    $result = mysqli_query($conn, $query1);
    $m30s = [];
    while ($m30 = mysqli_fetch_assoc($result)) {
        $m30s[] = $m30;
    };
    return $m30s;
}
//Function Tambah H4
function tambahh4($tambahh4)
{
    // Menngambil koneksi database dari variabel $conn diatas
    global $conn;
    // Masukan tiap data form pada variabel
    $pair = htmlspecialchars($tambahh4["pair"]);
    $waktuentry = htmlspecialchars($tambahh4["waktuentry"]);
    $tipetransaksi = htmlspecialchars($tambahh4["tipetransaksi"]);
    $entrypoint = htmlspecialchars($tambahh4["entrypoint"]);
    $closepoint = htmlspecialchars($tambahh4["closepoint"]);
    $hasil = htmlspecialchars($tambahh4["hasil"]);
    $jumlahpoint = htmlspecialchars($tambahh4["jumlahpoint"]);
    $dollar = htmlspecialchars($tambahh4["dollar"]);
    // sintaks query
    $query = "INSERT INTO teknikh4 
        VALUES ('','$pair','$waktuentry','$tipetransaksi','$entrypoint','$closepoint','$hasil','$jumlahpoint','$dollar')";

    // mengquery database
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}
//Function Tambah m30
function tambahm30($tambahm30)
{
    // Menngambil koneksi database dari variabel $conn diatas
    global $conn;
    // Masukan tiap data form pada variabel
    $pair = htmlspecialchars($tambahm30["pair"]);
    $waktuentry = htmlspecialchars($tambahm30["waktuentry"]);
    $tipetransaksi = htmlspecialchars($tambahm30["tipetransaksi"]);
    $entrypoint = htmlspecialchars($tambahm30["entrypoint"]);
    $closepoint = htmlspecialchars($tambahm30["closepoint"]);
    $hasil = htmlspecialchars($tambahm30["hasil"]);
    $jumlahpoint = htmlspecialchars($tambahm30["jumlahpoint"]);
    $dollar = htmlspecialchars($tambahm30["dollar"]);
    // sintaks query
    $query = "INSERT INTO teknikm30 
        VALUES ('','$pair','$waktuentry','$tipetransaksi','$entrypoint','$closepoint','$hasil','$jumlahpoint','$dollar')";

    // mengquery database
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}
// Function hapus
function hapush4($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM teknikh4 WHERE id = $id");
    return mysqli_affected_rows($conn);
}

function hapusm30($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM teknikm30  WHERE id = $id");
    return mysqli_affected_rows($conn);
}
// Function edit H4
function edith4($data)
{
    // Menngambil koneksi database dari variabel $conn diatas
    global $conn;
    // Masukan tiap data form pada variabel
    $id = $data["id"];
    $pair = $data["pair"];
    $waktuentry = $data["waktuentry"];
    $tipetransaksi = $data["tipetransaksi"];
    $entrypoint = $data["entrypoint"];
    $closepoint = $data["closepoint"];
    $hasil = $data["hasil"];
    $jumlahpoint = $data["jumlahpoint"];
    $dollar = $data["dollar"];
    // sintaks query
    $query = "UPDATE teknikh4 SET 
                pair = '$pair', 
                waktuentry = '$waktuentry', 
                tipetransaksi = '$tipetransaksi', 
                entrypoint = '$entrypoint', 
                closepoint = '$closepoint', 
                hasil = '$hasil', 
                jumlahpoint = '$jumlahpoint', 
                dollar = '$dollar' 
                WHERE id = $id";
    // mengquery database
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}
// Function edit M30
function editm30($data)
{
    // Menngambil koneksi database dari variabel $conn diatas
    global $conn;
    // Masukan tiap data form pada variabel
    $id = $data["id"];
    $pair = $data["pair"];
    $waktuentry = $data["waktuentry"];
    $tipetransaksi = $data["tipetransaksi"];
    $entrypoint = $data["entrypoint"];
    $closepoint = $data["closepoint"];
    $hasil = $data["hasil"];
    $jumlahpoint = $data["jumlahpoint"];
    $dollar = $data["dollar"];
    // sintaks query
    $query = "UPDATE teknikm30 SET 
                pair = '$pair', 
                waktuentry = '$waktuentry', 
                tipetransaksi = '$tipetransaksi', 
                entrypoint = '$entrypoint', 
                closepoint = '$closepoint', 
                hasil = '$hasil', 
                jumlahpoint = '$jumlahpoint', 
                dollar = '$dollar' 
                WHERE id = $id";
    // mengquery database
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}
// Function mencari h4
function carih4($keyword)
{
    // Masukan tiap data form pada variabel
    $query = "SELECT * FROM teknikh4 WHERE 
                pair LIKE '%$keyword%' OR
                waktuentry LIKE '%$keyword%' OR 
                tipetransaksi LIKE '%$keyword%' OR
                entrypoint LIKE '%$keyword%' OR
                closepoint LIKE '%$keyword%' OR
                hasil LIKE '%$keyword%' OR
                jumlahpoint LIKE '%$keyword%' OR
                dollar LIKE '%$keyword%'";
    // mengambil function query h4 yang sudah dibuat sebelumnya
    return queryh4($query);
}
// Function mencari m30
function carim30($keyword)
{
    // Masukan tiap data form pada variabel
    $query = "SELECT * FROM teknikm30 WHERE 
                pair LIKE '%$keyword%' OR
                waktuentry LIKE '%$keyword%' OR 
                tipetransaksi LIKE '%$keyword%' OR
                entrypoint LIKE '%$keyword%' OR
                closepoint LIKE '%$keyword%' OR
                hasil LIKE '%$keyword%' OR
                jumlahpoint LIKE '%$keyword%' OR
                dollar LIKE '%$keyword%'";
    // mengambil function query h4 yang sudah dibuat sebelumnya
    return querym30($query);
}

function daftar($data)
{
    // Memanggil function global conn query
    global $conn;
    //Memasukan data input ke variable
    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($conn, $data["password"]);
    $konfirmasipassword = mysqli_real_escape_string($conn, $data["konfirmasipassword"]);
    $broker = htmlspecialchars($data["broker"]);
    $email = htmlspecialchars($data["email"]);

    // Cek apakah username sudah pernah terdaftar atau belum
    $akun = mysqli_query($conn, "SELECT username FROM user WHERE username = '$username'");
    if (mysqli_fetch_assoc($akun)) {
        echo "  
            <script>
            alert('Username sudah pernah terdaftar');
            </script>";
        return false;
    }
    // Cek apakah password dan konfirmasinya sama
    if ($password !== $konfirmasipassword) {
        echo "
            <script>
                alert('Password dan Konfirmasi password tidak sama');
            </script>
            ";
        return false;
    }
    // Enkripsi password 
    $password = (password_hash($password, PASSWORD_DEFAULT));

    // Query kedatabase
    mysqli_query($conn, "INSERT INTO user VALUES ('0', '$username', '$password', '$broker', '$email')");

    return mysqli_affected_rows($conn);
}
