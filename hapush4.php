<?php
// Menjalankan fungsi session
session_start();
// jika session masuk belum dibuat maka kembalikan kehalaman masuk
if (!isset($_SESSION["masuk"])) {
    header("location: masuk.php");
    exit;
}
// Memanggil file function
require 'function/functions.php';
// menerima nilai pada variable $_GET
$id = $_GET["id"];

if (hapush4($id)) {
    echo "
        <script>
        alert('Data berhasil dihapus');
        document.location.href = 'index.php';
        </script>";
} else {
    echo "
    <script>
    alert('Data gagal dihapus');
    document.location.href = 'index.php';
    </script>
    ";
    echo "<br>";
    echo "mysqli_error($id);";
}
