<?php
// Menjalankan fungsi session
session_start();
// Jika session masuk sudah belum dibuat maka paksa user ke halaman masuk
if (!isset($_SESSION["masuk"])) {
    header("location: masuk.php");
    exit;
}
//Menghubungkan dengan file functions
require 'function/functions.php';
//mengambil data dari databases teknikh4
$h4s = queryh4("SELECT * FROM teknikh4");
//mengambil data dari databases teknikh4
$m30s = querym30("SELECT * FROM teknikm30");

//Jika tombol cari ditekan h4
if (isset($_POST["carih4"])) {
    $h4s = carih4($_POST["keywordh4"]);
}
//Jika tombol cari ditekan m30
if (isset($_POST["carim30"])) {
    $m30s = carim30($_POST["keywordm30"]);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/index.css">
    <title>Rekapitulasi Trading Manual</title>
</head>

<body>
    <!-- Tabel pertama -->
    <div class="container">
        <h1>Rekapitulasi Trading Manual</h1>
        <h2>Teknik H4</h2>
        <a href=" tambahh4.php"><button type="button" class="btn btn-secondary" id="tambahh4">Tambah transaksi</button></a>
        <form class="row g-3" action="" method="POST">
            <div class="col-auto">
                <input type="search" class="form-control" placeholder="Masukan keyword pencarian" name="keywordh4" size="40px" autofocus autocomplete="off"></input>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-3" name="carih4">Cari</button>
            </div>
        </form>
        <table class="table table-light table-hover table table-bordered table-responsive">
            <thead class=" table-dark">
                <tr>
                    <th>No</th>
                    <th>Aksi</th>
                    <th>Pair</th>
                    <th>Waktu Entry</th>
                    <th>Tipe Transaksi</th>
                    <th>Entry Point</th>
                    <th>Close Point</th>
                    <th>Hasil</th>
                    <th>Jumlah Point</th>
                    <th>Dollar</th>
                </tr>
            </thead> <?php $i = 1; ?>
            <?php foreach ($h4s as $h4) : ?>
                <tr class="data">
                    <td class="no"><?= $i; ?> </td>
                    <td class="aksi">
                        <div class="d-grid gap-2 d-md-block">
                            <a class="btn btn-outline-secondary" href="edith4.php?id=<?= $h4["id"] ?> " role="button" onclick="return confirm('apakah data ini ingin diubah?');">Ubah</a>
                            <a class="btn btn-outline-secondary" href="hapush4.php?id=<?= $h4["id"]; ?>" role="button" onclick="return confirm('apakah data ini ingin dihapus?');">Hapus</a>
                        </div>
                    </td>
                    <td><?= $h4["pair"]; ?></td>
                    <td><?= $h4["waktuentry"]; ?></td>
                    <td><?= $h4["tipetransaksi"]; ?></td>
                    <td><?= $h4["entrypoint"]; ?></td>
                    <td><?= $h4["closepoint"]; ?></td>
                    <td><?= $h4["hasil"]; ?></td>
                    <td><?= $h4["jumlahpoint"]; ?></td>
                    <td><?= $h4["dollar"]; ?></td>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
        </table>
    </div>
    <!-- Tabel ke dua -->
    <div class=" container">
        <h2>Teknik M30</h2>
        <a href="tambahm30.php"><button type="button" class="btn btn-secondary" id="tambahh4">Tambah transaksi</button></a>
        <form class="row g-3" action="" method="POST">
            <div class="col-auto">
                <input type="search" class="form-control" placeholder="Masukan keyword pencarian" name="keywordm30" size="40px" autofocus autocomplete="off"></input>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-3" name="carim30">Cari</button>
            </div>
        </form>
        <table class="table table-light table-hover table table-bordered table-responsive">
            <thead class="table-dark">
                <tr>
                    <th>No</th>
                    <th>Aksi</th>
                    <th>Pair</th>
                    <th>Waktu Entry</th>
                    <th>Tipe Transaksi</th>
                    <th>Entry Point</th>
                    <th>Close Point</th>
                    <th>Hasil</th>
                    <th>Jumlah Point</th>
                    <th>Dollar</th>
                </tr>
            </thead>
            <?php $i = 1; ?>
            <?php foreach ($m30s as $m30) : ?>
                <tr class="data">
                    <td class="no"><?= $i; ?> </td>
                    <td class="aksi">
                        <div class="d-grid gap-2 d-md-block">
                            <a class="btn btn-outline-secondary" href="editm30.php?id=<?= $m30["id"] ?> " role="button" onclick="return confirm('apakah data ini ingin diubah?');">Ubah</a>
                            <a class="btn btn-outline-secondary" href="hapusm30.php?id=<?= $m30["id"]; ?>" role="button" onclick="return confirm('apakah data ini ingin dihapus?');">Hapus</a>
                        </div>
                    </td>
                    <td><?= $m30["pair"]; ?></td>
                    <td><?= $m30["waktuentry"]; ?></td>
                    <td><?= $m30["tipetransaksi"]; ?></td>
                    <td><?= $m30["entrypoint"]; ?></td>
                    <td><?= $m30["closepoint"]; ?></td>
                    <td><?= $m30["hasil"]; ?></td>
                    <td><?= $m30["jumlahpoint"]; ?></td>
                    <td><?= $m30["dollar"]; ?></td>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
        </table>
        <a href="keluar.php"><button type="button" class="btn btn-primary" class="kembali">Keluar</button></a>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>