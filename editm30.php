<?php
// Menjalankan fungsi session
session_start();
// jika session masuk belum dibuat maka kembalikan kehalaman masuk
if (!isset($_SESSION["masuk"])) {
    header("location: masuk.php");
    exit;
}
// Menghubungkan ke file function
require 'function/functions.php';
// Ambil id dari index
$idm30 = $_GET["id"];
// Query data H4
$data = queryh4("SELECT * FROM teknikm30 WHERE id = $idm30")[0];
// cek apakah data sudah disubmit
if (isset($_POST["submit"])) {
    //Cek dan lakukan jika function tambah yang didapat dari $_POST mengembalikan nilai diatas 0 
    if (editm30($_POST) > 0) {
        echo "
            <script>
            alert('Data berhasil diedit');
            document.location.href = 'index.php';
            </script>";
    } else {
        echo "
        <script>
        alert('Data tidak berhasil diedit');
        document.location.href = 'edit4.php';
        </script>
        ";
        echo "<br>";
        echo "mysqli_error($_POST);";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/ubah.css">
    <title>Edit transaksi M30</title>
</head>

<body>
    <br>
    <div class="container">
        <h1>Edit transaksi M30</h1><br>
        <form action="" method="POST">
            <!--  -->
            <input type="hidden" name="id" id="id" value="<?= $data["id"]; ?>"></input>
            <div class="row">
                <div class="col-3">
                    <label for="pair" class="form-label">Pair</label>
                    <input type="text" class="form-control" id="pair" name="pair" required autocomplete="off" autofocus value="<?= $data["pair"]; ?>"></input>
                    <div id=" pair" class="form-text">Masukan pair yang ditransaksikan ex : USDJPY
                    </div>
                </div>
                <div class="col-3">
                    <label for="waktuentry" class="form-label">Waktu Entry</label>
                    <input type="text" class="form-control" id="waktuentry" name="waktuentry" required autocomplete="off" value="<?= $data["waktuentry"]; ?>"></input>
                    <div id="pair" class="form-text">Masukan waktu entry yang ditransaksikan ex : Sabtu 20-05-2021</div>
                </div>
            </div>
            <!--  -->
            <div class="row">
                <div class="col-3">
                    <label for="tipetransaksi" class="form-label">Tipe Transaksi</label>
                    <input type="text" class="form-control" id="tipetransaksi" name="tipetransaksi" required autocomplete="off" value="<?= $data["tipetransaksi"]; ?>"></input>
                    <div id=" pair" class="form-text">Masukan tipe transaksi yang ditransaksikan ex : Sell
                    </div>
                </div>
                <div class="col-3">
                    <label for="entrypoint" class="form-label">Entry Point</label>
                    <input type="text" class="form-control" id="entrypoint" name="entrypoint" required autocomplete="off" value="<?= $data["entrypoint"]; ?>"></input>
                    <div id="pair" class="form-text">Masukan entry point yang ditransaksikan ex : 190003</div>
                </div>
            </div>
            <!--  -->
            <div class="row">
                <div class="col-3">
                    <label for="closepoint" class="form-label">Close Point</label>
                    <input type="text" class="form-control" id="closepoint" name="closepoint" required autocomplete="off" value="<?= $data["closepoint"]; ?>"></input>
                    <div id="pair" class="form-text">Masukan close point yang ditransaksikan ex : 390032</div>
                </div>
                <div class="col-3">
                    <label for="hasil" class="form-label">Hasil</label>
                    <input type="text" class="form-control" id="hasil" name="hasil" required autocomplete="off" value="<?= $data["hasil"]; ?>"></input>
                    <div id="pair" class="form-text">Masukan hasil yang ditransaksikan ex : TP</div>
                </div>
            </div>
            <!--  -->
            <div class="row">
                <div class="col-3">
                    <label for="jumlahpoint" class="form-label">Jumlah Point</label>
                    <input type="text" class="form-control" id="jumlahpoint" name="jumlahpoint" required autocomplete="off" value="<?= $data["jumlahpoint"]; ?>"></input>
                    <div id="pair" class="form-text">Masukan pair yang ditransaksikan ex : 100</div>
                </div>
                <div class="col-3">
                    <label for="dollar" class="form-label">Dollar</label>
                    <input type="text" class="form-control" id="dollar" name="dollar" required autocomplete="off" value="<?= $data["dollar"]; ?>"></input>
                    <div id="pair" class="form-text">Masukan pair yang ditransaksikan ex : 1.00</div>
                </div>
            </div><br>
            <!--  -->
            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
            <!--  -->
            <a href="index.php"><button type="button" name="back" class="btn btn-danger">Batal</button> </a>
        </form>
        <div>
</body>

</html>