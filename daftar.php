<?php
// Menghubungkan dengan file functions
require 'function/functions.php';
// Cek apakah tombol daftar sudah ditekan
if (isset($_POST["submit"])) {
    // Cek apakah fungsi daftar mengembalikan nilai diatas 0 jika true
    if (daftar($_POST) > 0) {
        echo "  <script>
                alert('Selamat, Akun berhasil ditambahkan');
                document.location.href = 'masuk.php';
                </script>";
    } else {
        echo "
            <script>
            alert('Akun gagal ditambahkan!');
            </script>";
        echo "<br>";
        echo mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="css/daftar1.css">
    <title>Buat Akun</title>
</head>

<body>
    <h1>BUAT AKUN</h1>
    <form action="" method="POST">
        <div class="container">
            <!-- Baris username -->
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" required placeholder="Username" autofocus autocomplete="off" name="username">
                </div>

            </div>
            <!-- Baris password -->
            <div class="row row-cols-2 ">
                <div class="col-sm-3 offset-sm-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password" required autocomplete="off" name="password" required>
                </div>
                <div class="col-sm-3">
                    <label for="konfirmasipassword" class="form-label">Konfirmasi Password</label>
                    <input type="password" class="form-control" id="konfirmasipassword" placeholder="Konfirmasi Password" autocomplete="off" name="konfirmasipassword" required>
                </div>
            </div>
            <!-- Baris broker dan email -->
            <div class="row row-cols-2 ">
                <div class="col-sm-3 offset-sm-3">
                    <label for="broker" class="form-label">Broker</label>
                    <input type="text" class="form-control" id="broker" placeholder="Nama Broker" autocomplete="off" name="broker" required>
                </div>
                <div class="col-sm-3">
                    <label for="email" class="form-label">Alamat E-mail</label>
                    <input type="email" class="form-control" id="email" placeholder="Alamat E-mail" autocomplete="off" name="email" required>
                </div>
            </div>
            <!-- Tombol -->
            <div class="row">
                <div class="col-3 offset-sm-3">
                    <button type="submit" class="btn btn-primary" name="submit">Daftar</button>
                </div>
            </div>
        </div>
    </form>

</body>

</html>